package com.silvioapps.omdbsample.interfaces;

import android.view.View;

public interface IRecyclerViewOnClickListener
{
    void recyclerViewListClicked(View v, int position);
}

