package com.silvioapps.omdbsample.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.silvioapps.omdbsample.constants.Constants;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class Search implements Parcelable {
    @SerializedName("Search")
    private List<Result> search = null;
    @SerializedName("totalResults")
    private int totalResults = -1;
    @SerializedName("Response")
    private boolean response = false;

    public interface API {
        @GET(Constants.QUERY)
        Call<Search> get(@Query("s") String search);
    }

    public List<Result> getSearch() {
        return search;
    }

    public void setSearch(List<Result> search) {
        this.search = search;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public boolean getResponse() {
        return response;
    }

    public void setResponse(boolean response) {
        this.response = response;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.search);
        dest.writeInt(this.totalResults);
        dest.writeByte(this.response ? (byte) 1 : (byte) 0);
    }

    public Search() {
    }

    protected Search(Parcel in) {
        this.search = new ArrayList<Result>();
        in.readList(this.search, Result.class.getClassLoader());
        this.totalResults = in.readInt();
        this.response = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Search> CREATOR = new Parcelable.Creator<Search>() {
        @Override
        public Search createFromParcel(Parcel source) {
            return new Search(source);
        }

        @Override
        public Search[] newArray(int size) {
            return new Search[size];
        }
    };
}
