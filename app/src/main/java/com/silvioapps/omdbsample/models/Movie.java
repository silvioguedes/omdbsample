package com.silvioapps.omdbsample.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.silvioapps.omdbsample.constants.Constants;

import io.realm.RealmObject;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class Movie extends RealmObject implements Parcelable {
    @SerializedName("Title")
    private String title = null;
    @SerializedName("Year")
    private String year = null;
    @SerializedName("Rated")
    private String rated = null;
    @SerializedName("Released")
    private String released = null;
    @SerializedName("Runtime")
    private String runtime = null;
    @SerializedName("Genre")
    private String genres = null;
    @SerializedName("Director")
    private String directors = null;
    @SerializedName("Writer")
    private String writers = null;
    @SerializedName("Actors")
    private String actors = null;
    @SerializedName("Plot")
    private String plot = null;
    @SerializedName("Language")
    private String languages = null;
    @SerializedName("Country")
    private String countries = null;
    @SerializedName("Awards")
    private String awards = null;
    @SerializedName("Poster")
    private String poster = null;
    @SerializedName("Metascore")
    private String metascore = null;
    @SerializedName("imdbRating")
    private String imdbRating = null;
    @SerializedName("imdbVotes")
    private String imdbVotes = null;
    @SerializedName("imdbID")
    private String imdbID = null;
    @SerializedName("Type")
    private String type = null;
    @SerializedName("Response")
    private boolean response = false;

    public interface API {
        @GET(Constants.QUERY)
        Call<Movie> get(@Query("i") String imdbID);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getRated() {
        return rated;
    }

    public void setRated(String rated) {
        this.rated = rated;
    }

    public String getReleased() {
        return released;
    }

    public void setReleased(String released) {
        this.released = released;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public String getDirectors() {
        return directors;
    }

    public void setDirectors(String directors) {
        this.directors = directors;
    }

    public String getWriters() {
        return writers;
    }

    public void setWriters(String writers) {
        this.writers = writers;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public String getLanguages() {
        return languages;
    }

    public void setLanguages(String languages) {
        this.languages = languages;
    }

    public String getCountries() {
        return countries;
    }

    public void setCountries(String countries) {
        this.countries = countries;
    }

    public String getAwards() {
        return awards;
    }

    public void setAwards(String awards) {
        this.awards = awards;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getMetascore() {
        return metascore;
    }

    public void setMetascore(String metascore) {
        this.metascore = metascore;
    }

    public String getImdbRating() {
        return imdbRating;
    }

    public void setImdbRating(String imdbRating) {
        this.imdbRating = imdbRating;
    }

    public String getImdbVotes() {
        return imdbVotes;
    }

    public void setImdbVotes(String imdbVotes) {
        this.imdbVotes = imdbVotes;
    }

    public String getImdbID() {
        return imdbID;
    }

    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isResponse() {
        return response;
    }

    public void setResponse(boolean response) {
        this.response = response;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.year);
        dest.writeString(this.rated);
        dest.writeString(this.released);
        dest.writeString(this.runtime);
        dest.writeString(this.genres);
        dest.writeString(this.directors);
        dest.writeString(this.writers);
        dest.writeString(this.actors);
        dest.writeString(this.plot);
        dest.writeString(this.languages);
        dest.writeString(this.countries);
        dest.writeString(this.awards);
        dest.writeString(this.poster);
        dest.writeString(this.metascore);
        dest.writeString(this.imdbRating);
        dest.writeString(this.imdbVotes);
        dest.writeString(this.imdbID);
        dest.writeString(this.type);
        dest.writeByte(this.response ? (byte) 1 : (byte) 0);
    }

    public Movie() {
    }

    protected Movie(Parcel in) {
        this.title = in.readString();
        this.year = in.readString();
        this.rated = in.readString();
        this.released = in.readString();
        this.runtime = in.readString();
        this.genres = in.readString();
        this.directors = in.readString();
        this.writers = in.readString();
        this.actors = in.readString();
        this.plot = in.readString();
        this.languages = in.readString();
        this.countries = in.readString();
        this.awards = in.readString();
        this.poster = in.readString();
        this.metascore = in.readString();
        this.imdbRating = in.readString();
        this.imdbVotes = in.readString();
        this.imdbID = in.readString();
        this.type = in.readString();
        this.response = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Movie> CREATOR = new Parcelable.Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel source) {
            return new Movie(source);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };
}
