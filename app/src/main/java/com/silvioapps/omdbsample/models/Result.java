package com.silvioapps.omdbsample.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class Result extends RealmObject implements Parcelable {
    @SerializedName("Title")
    private String title = null;
    @SerializedName("Year")
    private String year = null;
    @SerializedName("imdbID")
    private String imdbID = null;
    @SerializedName("Type")
    private String type = null;
    @SerializedName("Poster")
    private String poster = null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getImdbID() {
        return imdbID;
    }

    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.year);
        dest.writeString(this.imdbID);
        dest.writeString(this.type);
        dest.writeString(this.poster);
    }

    public Result() {
    }

    protected Result(Parcel in) {
        this.title = in.readString();
        this.year = in.readString();
        this.imdbID = in.readString();
        this.type = in.readString();
        this.poster = in.readString();
    }

    public static final Parcelable.Creator<Result> CREATOR = new Parcelable.Creator<Result>() {
        @Override
        public Result createFromParcel(Parcel source) {
            return new Result(source);
        }

        @Override
        public Result[] newArray(int size) {
            return new Result[size];
        }
    };
}
