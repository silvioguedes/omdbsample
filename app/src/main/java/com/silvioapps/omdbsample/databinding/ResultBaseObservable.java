package com.silvioapps.omdbsample.databinding;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import com.silvioapps.omdbsample.BR;

public class ResultBaseObservable extends BaseObservable {
    private String poster = null;
    private String title = null;
    private String year = null;

    public ResultBaseObservable(String poster, String title, String year) {
        this.poster = poster;
        this.title = title;
        this.year = year;
    }

    @Bindable
    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
        notifyPropertyChanged(BR.poster);
    }

    @Bindable
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        notifyPropertyChanged(BR.title);
    }

    @Bindable
    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
        notifyPropertyChanged(BR.year);
    }
}
