package com.silvioapps.omdbsample.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.silvioapps.omdbsample.R;
import com.silvioapps.omdbsample.constants.Constants;
import com.silvioapps.omdbsample.database.DatabaseHandler;
import com.silvioapps.omdbsample.fragments.MoviesListFragment;
import com.silvioapps.omdbsample.models.Movie;
import com.silvioapps.omdbsample.models.Result;
import com.silvioapps.omdbsample.models.Search;
import com.silvioapps.omdbsample.preferences.CustomSharedPreferences;
import com.silvioapps.omdbsample.services.SearchIntentService;
import com.silvioapps.omdbsample.statics.Statics;
import com.silvioapps.omdbsample.tasks.LoadingAsyncTask;
import com.silvioapps.omdbsample.utils.Utils;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private MessengerReceiver messengerReceiver = null;
    private MoviesListFragment moviesListFragment = null;
    private View actionBarLayout = null;
    private EditText editText = null;
    private LoadingAsyncTask loadingAsyncTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        messengerReceiver = new MessengerReceiver();
        loadingAsyncTask = new LoadingAsyncTask(this);

        toolbarSettings();

        attachFragment();

        //cleanDatabase();

        instantiateViews();

        search(CustomSharedPreferences.getSearch(this));
    }

    @Override
    protected void onResume(){
        super.onResume();
        if(messengerReceiver != null) {
            LocalBroadcastManager.getInstance(this).registerReceiver(messengerReceiver, new IntentFilter(Constants.BROADCAST_ACTION));
        }
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        if(messengerReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(messengerReceiver);
        }

        if(loadingAsyncTask != null) {
            loadingAsyncTask.finishLoadingAsyncTask();
        }
    }

    protected void toolbarSettings(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolBar);
        Utils.removeToolbarSidePaddings(toolbar);
        setSupportActionBar(toolbar);

        if(toolbar != null) {
            toolbar.setBackgroundColor(Color.TRANSPARENT);
        }

        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setDisplayShowTitleEnabled(false);

            actionBarLayout = getLayoutInflater().inflate(R.layout.search_action_bar_layout, null);

            if(actionBarLayout != null) {
                actionBar.setCustomView(actionBarLayout);

                ImageButton searchImageButton = (ImageButton) actionBarLayout.findViewById(R.id.action_search);
                if (searchImageButton != null) {
                    searchImageButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onSearch();
                        }
                    });
                }

                ImageButton cleanImageButton = (ImageButton) actionBarLayout.findViewById(R.id.cleanImageButton);
                if (cleanImageButton != null) {
                    cleanImageButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onClean();
                        }
                    });
                }
            }
        }
    }

    protected void instantiateViews(){
        editText = (EditText)findViewById(R.id.editText);
        if(editText != null) {
            editText.setText(CustomSharedPreferences.getSearch(this));

            Utils.setHintFont(editText, Constants.HINT_FONT);
        }
    }

    protected void onSearch(){
        if (editText.getText().length() > 0) {
            String title = editText.getText().toString();

            CustomSharedPreferences.setSearch(this, title);

            search(title);
        }
    }

    protected void onClean(){
        if(editText != null && editText.getText().length() > 0){
            editText.setText("");
        }
    }

    protected void attachFragment(){
        moviesListFragment = new MoviesListFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, moviesListFragment);
        fragmentTransaction.commit();
    }

    protected void cleanDatabase(){
        DatabaseHandler.deleteAllMovies();
        DatabaseHandler.deleteAllResults();
    }

    protected void search(String title){
        if(title != null && title.length() > 0) {
            if (loadingAsyncTask != null) {
                loadingAsyncTask.startLoadingAsyncTask();
            }

            List<Result> results = DatabaseHandler.getResults(title);
            if (results != null && results.size() > 0) {
                Statics.loadingResponse = true;

                if (moviesListFragment != null) {
                    moviesListFragment.setResults(results);
                }
            } else {
                Intent intent = new Intent(this, SearchIntentService.class);
                intent.putExtra(Constants.SEARCH_MOVIE, title);
                startService(intent);
            }
        }
    }

    private class MessengerReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent != null && intent.getAction().equals(Constants.BROADCAST_ACTION)){
                if(intent.hasExtra(Constants.RESULT_SEARCH)) {
                    Search search = intent.getParcelableExtra(Constants.RESULT_SEARCH);

                    Statics.loadingResponse = true;

                    if (search != null && search.getTotalResults() > 0) {
                        if (moviesListFragment != null) {
                            moviesListFragment.setResults(search.getSearch());
                        }
                    }
                    else if (search != null && search.getTotalResults() <= 0) {
                        Toast.makeText(MainActivity.this, R.string.no_results, Toast.LENGTH_SHORT).show();
                    }
                    else if (search == null) {
                        Toast.makeText(MainActivity.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                    }

                    intent.removeExtra(Constants.RESULT_SEARCH);
                }
                else if(intent.hasExtra(Constants.MOVIE_INFO)) {
                    Movie movie = intent.getParcelableExtra(Constants.MOVIE_INFO);

                    Statics.loadingResponse = true;

                    if (movie != null) {
                        Intent newIntent = new Intent(MainActivity.this, MovieInfoActivity.class);
                        newIntent.putExtra(Constants.MOVIE_PARCELABLE, movie);
                        startActivity(newIntent);
                    }
                    else{
                        Toast.makeText(MainActivity.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                    }

                    intent.removeExtra(Constants.MOVIE_INFO);
                }
            }
        }
    }
}
