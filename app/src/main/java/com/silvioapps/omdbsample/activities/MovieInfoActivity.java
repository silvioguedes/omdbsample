package com.silvioapps.omdbsample.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.silvioapps.omdbsample.R;
import com.silvioapps.omdbsample.constants.Constants;
import com.silvioapps.omdbsample.fragments.MovieInfoFragment;
import com.silvioapps.omdbsample.models.Movie;
import com.silvioapps.omdbsample.utils.Utils;

public class MovieInfoActivity extends AppCompatActivity {
    private MovieInfoFragment movieInfoFragment = null;
    private View actionBarLayout = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_info);

        Intent intent = getIntent();
        Movie movie = null;
        if(intent != null && intent.hasExtra(Constants.MOVIE_PARCELABLE)){
            movie = intent.getParcelableExtra(Constants.MOVIE_PARCELABLE);
        }

        toolbarSettings(movie);

        attachFragment(movie);
    }

    @Override
    protected void onResume(){
        super.onResume();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
    }

    protected void toolbarSettings(Movie movie){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolBar);
        Utils.removeToolbarSidePaddings(toolbar);
        setSupportActionBar(toolbar);

        if(toolbar != null) {
            toolbar.setBackgroundColor(Color.TRANSPARENT);
        }

        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setDisplayShowTitleEnabled(false);

            actionBarLayout = getLayoutInflater().inflate(R.layout.title_action_bar_layout, null);

            if(actionBarLayout != null) {
                actionBar.setCustomView(actionBarLayout);

                TextView titleTextView = (TextView)actionBarLayout.findViewById(R.id.textView);
                if(titleTextView != null && movie != null && movie.getTitle() != null){
                    String text = movie.getTitle() + " (" + movie.getYear() + ")";
                    titleTextView.setText(text);
                }

                ImageButton backImageButton = (ImageButton) actionBarLayout.findViewById(R.id.action_back);
                if (backImageButton != null) {
                    backImageButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onBack();
                        }
                    });
                }
            }
        }
    }

    protected void onBack(){
       finish();
    }

    protected void attachFragment(Movie movie){
        movieInfoFragment = new MovieInfoFragment();

        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.MOVIE_PARCELABLE, movie);

        movieInfoFragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, movieInfoFragment);
        fragmentTransaction.commit();
    }
}
