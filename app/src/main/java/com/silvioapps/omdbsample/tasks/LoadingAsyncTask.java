package com.silvioapps.omdbsample.tasks;

import android.os.AsyncTask;
import android.support.v4.os.AsyncTaskCompat;
import android.support.v7.app.AppCompatActivity;

import com.silvioapps.omdbsample.dialogs.LoadingDialogFragment;
import com.silvioapps.omdbsample.statics.Statics;

public class LoadingAsyncTask extends AsyncTask<Void, Void, Void> {
    private LoadingDialogFragment loadingDialogFragment = null;
    private LoadingAsyncTask loadingAsyncTask = null;
    private AppCompatActivity appCompatActivity = null;

    public LoadingAsyncTask(AppCompatActivity appCompatActivity){
        this.appCompatActivity = appCompatActivity;
    }

    @Override
    protected void onPreExecute(){
        Statics.loadingResponse = false;

        showLoadingDialogFragment();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        while(!isCancelled() && !Statics.loadingResponse){
            //TODO NOTHING
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void voids){
        hideLoadingDialogFragment();

        Statics.loadingResponse = false;
    }

    public void startLoadingAsyncTask(){
        finishLoadingAsyncTask();

        if(appCompatActivity != null) {
            loadingAsyncTask = new LoadingAsyncTask(appCompatActivity);
        }

        AsyncTaskCompat.executeParallel(loadingAsyncTask);
    }

    public void finishLoadingAsyncTask(){
        if(loadingAsyncTask != null){
            loadingAsyncTask.cancel(true);
            loadingAsyncTask = null;
        }
    }

    public void showLoadingDialogFragment(){
        if (loadingDialogFragment != null && loadingDialogFragment.isShowing()) {
            return;
        }

        if(appCompatActivity != null) {
            try {
                loadingDialogFragment = new LoadingDialogFragment();
                loadingDialogFragment.setCancelable(false);
                loadingDialogFragment.show(appCompatActivity.getSupportFragmentManager(), LoadingDialogFragment.FRAGMENT_TAG);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void hideLoadingDialogFragment(){
        if(loadingDialogFragment != null){
           loadingDialogFragment.cancel();
        }
    }
}
