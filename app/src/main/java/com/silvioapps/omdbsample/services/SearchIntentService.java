package com.silvioapps.omdbsample.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.IBinder;
import android.os.Parcelable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.silvioapps.omdbsample.constants.Constants;
import com.silvioapps.omdbsample.database.DatabaseHandler;
import com.silvioapps.omdbsample.models.Movie;
import com.silvioapps.omdbsample.models.Result;
import com.silvioapps.omdbsample.models.Search;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchIntentService extends IntentService {
    private Gson gson = null;
    private Retrofit retrofit = null;

    public SearchIntentService() {
        super("SearchIntentService");
    }

    @Override
    public void onCreate(){
        super.onCreate();

        gson = new GsonBuilder()
                .create();

        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startID) {
        if(intent != null) {
            if(intent.hasExtra(Constants.SEARCH_MOVIE)) {
                searchMovie(intent);

                intent.removeExtra(Constants.SEARCH_MOVIE);
            }
            else if(intent.hasExtra(Constants.GET_INFO_MOVIE) && intent.hasExtra(Constants.RESULT_PARCELABLE)) {
                getInfoMovie(intent);

                intent.removeExtra(Constants.GET_INFO_MOVIE);
                intent.removeExtra(Constants.RESULT_PARCELABLE);
            }
        }

        return START_NOT_STICKY;
    }

    protected void searchMovie(Intent intent){
        if(gson != null && retrofit != null) {
            final String movieName = intent.getStringExtra(Constants.SEARCH_MOVIE);

            Search.API service = retrofit.create(Search.API.class);
            Call<Search> call = service.get(movieName);
            call.enqueue(new Callback<Search>() {
                @Override
                public void onResponse(Call<Search> call, retrofit2.Response<Search> response) {
                    Search search = response.body();

                    DatabaseHandler.saveResults(search);

                    Intent localIntent = new Intent(Constants.BROADCAST_ACTION);
                    localIntent.putExtra(Constants.RESULT_SEARCH, search);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(localIntent);
                }

                @Override
                public void onFailure(Call<Search> call, Throwable t) {
                    Log.e("TAG***", "onFailure " + t.getMessage());

                    Intent localIntent = new Intent(Constants.BROADCAST_ACTION);
                    localIntent.putExtra(Constants.RESULT_SEARCH, (Parcelable)null);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(localIntent);
                }
            });
        }
    }

    protected void getInfoMovie(Intent intent){
        int index = intent.getIntExtra(Constants.GET_INFO_MOVIE, -1);
        ArrayList<Result> result = intent.getParcelableArrayListExtra(Constants.RESULT_PARCELABLE);

        if(result != null && index > -1 && gson != null && retrofit != null) {
            String imdbID = result.get(index).getImdbID();

            Movie.API service2 = retrofit.create(Movie.API.class);
            Call<Movie> call2 = service2.get(imdbID);
            call2.enqueue(new Callback<Movie>() {
                @Override
                public void onResponse(Call<Movie> call, retrofit2.Response<Movie> response) {
                    Movie movie = response.body();

                    DatabaseHandler.saveMovie(movie);

                    Intent localIntent = new Intent(Constants.BROADCAST_ACTION);
                    localIntent.putExtra(Constants.MOVIE_INFO, movie);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(localIntent);
                }

                @Override
                public void onFailure(Call<Movie> call, Throwable t) {
                    Log.e("TAG***", "onFailure " + t.getMessage());

                    Intent localIntent = new Intent(Constants.BROADCAST_ACTION);
                    localIntent.putExtra(Constants.MOVIE_INFO, (Parcelable)null);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(localIntent);
                }
            });
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        if(intent != null) {
           //TODO
        }

        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent){
        if(intent != null) {
            //TODO
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return false;
    }
}