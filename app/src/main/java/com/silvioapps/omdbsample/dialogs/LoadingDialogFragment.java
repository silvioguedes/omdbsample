package com.silvioapps.omdbsample.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.silvioapps.omdbsample.R;

import javax.annotation.Nonnull;

public class LoadingDialogFragment extends DialogFragment {
    public static final String FRAGMENT_TAG = "LoadingDialogFragment";
    private boolean isShowing = false;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }

    @Override@Nonnull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = null;

        if(getActivity() != null) {
            dialog = new Dialog(getActivity());

            if(dialog.getWindow() != null) {
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

                if (!dialog.getWindow().hasFeature(Window.FEATURE_NO_TITLE)) {
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                }
            }

            dialog.setContentView(R.layout.dialog_fragment_loading);
        }

        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle){
        View view = layoutInflater.inflate(R.layout.dialog_fragment_loading, viewGroup, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
    }

    @Override
    public void onStart(){
        super.onStart();
        isShowing = true;
    }

    @Override
    public void onResume(){
        super.onResume();
        isShowing = true;
    }

    @Override
    public void onPause(){
        super.onPause();
        isShowing = false;
    }

    @Override
    public void onStop(){
        super.onStop();
        isShowing = false;
    }

    @Override
    public void onDestroyView(){
        Dialog dialog = getDialog();
        // handles https://code.google.com/p/android/issues/detail?id=17423
        if (dialog != null && getRetainInstance()) {
            dialog.setDismissMessage(null);
        }

        super.onDestroyView();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    @Override
    public void onDetach(){
        super.onDetach();
    }

    public boolean isShowing(){
        return isShowing;
    }

    public void cancel(){
        if(getActivity() != null && !getActivity().isFinishing()) {
            try {
                dismiss();
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }
    }
}
