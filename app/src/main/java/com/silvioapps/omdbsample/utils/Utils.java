package com.silvioapps.omdbsample.utils;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Typeface;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.TypefaceSpan;
import android.util.DisplayMetrics;
import android.widget.EditText;

public class Utils {
    public static void removeToolbarSidePaddings(Toolbar toolbar) {
        if (toolbar != null) {
            toolbar.setContentInsetsAbsolute(0, 0);
            toolbar.setPadding(0, 0, 0, 0);
        }
    }

    public static void setHintFont(final EditText editText, final String font){
        if(editText.getHint() != null) {
            Typeface typeface = Typeface.createFromAsset(editText.getContext().getAssets(), font);
            TypefaceSpan typefaceSpan = new CustomTypefaceSpan(typeface);
            SpannableString spannableString = new SpannableString(editText.getHint());
            spannableString.setSpan(typefaceSpan, 0, spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            editText.setHint(spannableString);
        }
    }
}
