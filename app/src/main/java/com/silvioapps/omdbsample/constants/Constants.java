package com.silvioapps.omdbsample.constants;

public class Constants {
    public static String SEARCH_MOVIE = "SEARCH_MOVIE";
    public static String RESULT_SEARCH = "RESULT_SEARCH";
    public static String MOVIE_INFO = "MOVIE_INFO";
    public static final String GET_INFO_MOVIE = "GET_INFO_MOVIE";
    public static final String RESULT_PARCELABLE = "RESULT_PARCELABLE";
    public static final String MOVIE_PARCELABLE = "MOVIE_PARCELABLE";
    public static final String BROADCAST_ACTION = "BROADCAST_ACTION";
    public static final String BASE_URL = "http://www.omdbapi.com";
    public static final String QUERY = "/?";
    public static final int WIDTH_IMAGE_THUMBNAIL = 75;
    public static final int HEIGHT_IMAGE_THUMBNAIL = 100;
    public static final String SHARED_PREFERENCES = "SHARED_PREFERENCES";
    public static final String HINT_FONT = "fonts/Roboto-Regular.ttf";
    public static final long SPLASH_SCREEN_SLEEP = 2500;
}
