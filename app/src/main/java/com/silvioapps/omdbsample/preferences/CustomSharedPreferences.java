package com.silvioapps.omdbsample.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.silvioapps.omdbsample.R;
import com.silvioapps.omdbsample.constants.Constants;

public class CustomSharedPreferences {

    public static void setSearch(Context context, String search){
        SharedPreferences sharedPref = context.getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(context.getString(R.string.search_preferences), search);

        editor.apply();
    }

    public static String getSearch(Context context){
        SharedPreferences sharedPref = context.getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE);

        return sharedPref.getString(context.getString(R.string.search_preferences), null);
    }
}
