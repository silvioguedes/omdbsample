package com.silvioapps.omdbsample.database;

import com.silvioapps.omdbsample.models.Movie;
import com.silvioapps.omdbsample.models.Result;
import com.silvioapps.omdbsample.models.Search;

import java.util.ArrayList;
import java.util.List;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.exceptions.RealmMigrationNeededException;

public class DatabaseHandler {

    private static Realm getRealm(){
        RealmConfiguration realmConfiguration = new RealmConfiguration
                .Builder()
                .build();

        Realm realm = null;
        try {
            realm = Realm.getInstance(realmConfiguration);
        } catch (RealmMigrationNeededException e){
            try {
                Realm.deleteRealm(realmConfiguration);
                realm = Realm.getInstance(realmConfiguration);
            } catch (Exception ex){
                throw ex;
            }
        }

        return realm;
    }

    public static void saveMovie(final Movie obj){
        Realm realm = getRealm();
        if(obj != null) {
            try{
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        Movie movie = realm.createObject(Movie.class);
                        movie.setActors(obj.getActors());
                        movie.setAwards(obj.getAwards());
                        movie.setCountries(obj.getCountries());
                        movie.setDirectors(obj.getDirectors());
                        movie.setGenres(obj.getGenres());
                        movie.setImdbID(obj.getImdbID());
                        movie.setImdbRating(obj.getImdbRating());
                        movie.setImdbVotes(obj.getImdbVotes());
                        movie.setLanguages(obj.getLanguages());
                        movie.setMetascore(obj.getMetascore());
                        movie.setPlot(obj.getPlot());
                        movie.setPoster(obj.getPoster());
                        movie.setRated(obj.getRated());
                        movie.setReleased(obj.getReleased());
                        movie.setResponse(obj.isResponse());
                        movie.setRuntime(obj.getRuntime());
                        movie.setTitle(obj.getTitle());
                        movie.setType(obj.getType());
                        movie.setWriters(obj.getWriters());
                        movie.setYear(obj.getYear());
                    }
                });
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    public static Movie getMovie(String imdbID){
        Realm realm = getRealm();

        Movie obj = realm.where(Movie.class).equalTo("imdbID",imdbID).findFirst();

        return obj;
    }

    public static void deleteAllMovies(){
        Realm realm = getRealm();

        RealmResults<Movie> realmResults = realm.where(Movie.class).findAll();
        try{
            realm.beginTransaction();

            realmResults.deleteAllFromRealm();

            realm.commitTransaction();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public static void saveResults(Search search){
        Realm realm = getRealm();
        if(search != null) {
            try{
                for (final Result obj : search.getSearch()) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            Result result = realm.createObject(Result.class);
                            result.setImdbID(obj.getImdbID());
                            result.setPoster(obj.getPoster());
                            result.setTitle(obj.getTitle());
                            result.setType(obj.getType());
                            result.setYear(obj.getYear());
                        }
                    });
                }
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    public static List<Result> getResults(String title){
        Realm realm = getRealm();

        List<Result> obj = new ArrayList<>(realm.where(Result.class).contains("title",title,Case.INSENSITIVE).findAll());

        return obj;
    }

    public static void deleteAllResults(){
        Realm realm = getRealm();

        RealmResults<Result> realmResults = realm.where(Result.class).findAll();
        try{
            realm.beginTransaction();

            realmResults.deleteAllFromRealm();

            realm.commitTransaction();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
