package com.silvioapps.omdbsample.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.silvioapps.omdbsample.R;
import com.silvioapps.omdbsample.constants.Constants;
import com.silvioapps.omdbsample.models.Movie;
import com.squareup.picasso.Picasso;

public class MovieInfoFragment extends Fragment {

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);

        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle){
        View view = layoutInflater.inflate(R.layout.fragment_movie_info, viewGroup, false);

        Movie movie = getArguments().getParcelable(Constants.MOVIE_PARCELABLE);
        if (movie != null) {
            ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
            Picasso.with(imageView.getContext())
                    .load(movie.getPoster())
                    .placeholder(R.drawable.placeholder)
                    .into(imageView);

            TextView textView = (TextView) view.findViewById(R.id.titleYearTextView);
            String text = movie.getTitle() + " (" + movie.getYear() + ")";
            textView.setText(text);

            textView = (TextView) view.findViewById(R.id.ratedRuntimeTextView);
            text = getString(R.string.rated_concatenate) + movie.getRated() + getString(R.string.runtime_concatenate) + movie.getRuntime();
            textView.setText(text);

            textView = (TextView) view.findViewById(R.id.genreReleaseTextView);
            text = getString(R.string.genre_concatenate) + movie.getGenres() + getString(R.string.release_concatenate) + movie.getReleased();
            textView.setText(text);

            textView = (TextView) view.findViewById(R.id.imdbRatingVotesTextView);
            text = getString(R.string.imdbRating_concatenate) + movie.getImdbRating() + getString(R.string.of) + movie.getImdbVotes() + getString(R.string.imdbVotes_concatenate);
            textView.setText(text);

            textView = (TextView) view.findViewById(R.id.plotTextView);
            text = getString(R.string.plot) + movie.getPlot();
            textView.setText(text);

            textView = (TextView) view.findViewById(R.id.directorTextView);
            text = getString(R.string.directors) + movie.getDirectors();
            textView.setText(text);

            textView = (TextView) view.findViewById(R.id.writerTextView);
            text = getString(R.string.writers) + movie.getWriters();
            textView.setText(text);

            textView = (TextView) view.findViewById(R.id.actorsTextView);
            text = getString(R.string.actors) + movie.getActors();
            textView.setText(text);

            textView = (TextView) view.findViewById(R.id.typeTextView);
            text = getString(R.string.type) + movie.getType();
            textView.setText(text);

            textView = (TextView) view.findViewById(R.id.metascoreTextView);
            text = getString(R.string.metascore) + movie.getMetascore();
            textView.setText(text);

            textView = (TextView) view.findViewById(R.id.awardsTextView);
            text = getString(R.string.awards) + movie.getAwards();
            textView.setText(text);

            textView = (TextView) view.findViewById(R.id.countryTextView);
            text = getString(R.string.countries) + movie.getCountries();
            textView.setText(text);

            textView = (TextView) view.findViewById(R.id.languageTextView);
            text = getString(R.string.languages) + movie.getLanguages();
            textView.setText(text);
        }

        return view;
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }

    @Override
    public void onDestroy(){
        if(getActivity() != null && !getActivity().isChangingConfigurations()) {
            //TODO
        }

        super.onDestroy();
    }

    @Override
    public void onDetach(){
        super.onDetach();
    }
}
