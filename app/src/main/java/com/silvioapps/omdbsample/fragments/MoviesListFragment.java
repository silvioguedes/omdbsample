package com.silvioapps.omdbsample.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.silvioapps.omdbsample.R;
import com.silvioapps.omdbsample.activities.MovieInfoActivity;
import com.silvioapps.omdbsample.adapters.MoviesListAdapter;
import com.silvioapps.omdbsample.constants.Constants;
import com.silvioapps.omdbsample.database.DatabaseHandler;
import com.silvioapps.omdbsample.databinding.ResultBaseObservable;
import com.silvioapps.omdbsample.interfaces.IRecyclerViewOnClickListener;
import com.silvioapps.omdbsample.models.Movie;
import com.silvioapps.omdbsample.models.Result;
import com.silvioapps.omdbsample.preferences.CustomSharedPreferences;
import com.silvioapps.omdbsample.services.SearchIntentService;
import com.silvioapps.omdbsample.statics.Statics;
import com.silvioapps.omdbsample.tasks.LoadingAsyncTask;

import java.util.ArrayList;
import java.util.List;

public class MoviesListFragment extends Fragment implements IRecyclerViewOnClickListener {
    private RecyclerView recyclerView = null;
    private MoviesListAdapter moviesListAdapter = null;
    private List<ResultBaseObservable> resultBaseObservableList = null;
    private LinearLayoutManager linearLayoutManager = null;
    private LoadingAsyncTask loadingAsyncTask = null;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);

        setRetainInstance(true);

        loadingAsyncTask = new LoadingAsyncTask((AppCompatActivity)getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle){
        View view = layoutInflater.inflate(R.layout.fragment_movies_list, viewGroup, false);

        if(getActivity() != null) {
            linearLayoutManager = new LinearLayoutManager(getActivity());

            instantiateList();

            recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(moviesListAdapter);
        }

        return view;
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }

    @Override
    public void onDestroy(){
        if(getActivity() != null && !getActivity().isChangingConfigurations()) {
            if(loadingAsyncTask != null) {
                loadingAsyncTask.finishLoadingAsyncTask();
            }
        }

        super.onDestroy();
    }

    @Override
    public void onDetach(){
        super.onDetach();
    }

    @Override
    public void recyclerViewListClicked(View v, int position) {
        if(getActivity() != null) {
            if(loadingAsyncTask != null) {
                loadingAsyncTask.startLoadingAsyncTask();
            }

            String title = CustomSharedPreferences.getSearch(getActivity());

            List<Result> results = DatabaseHandler.getResults(title);
            if (results != null && results.size() > 0) {
                Movie movie = DatabaseHandler.getMovie(results.get(position).getImdbID());

                if (movie != null && movie.getTitle() != null) {
                    Intent intent = new Intent(getActivity(), MovieInfoActivity.class);
                    intent.putExtra(Constants.MOVIE_PARCELABLE, movie);
                    startActivity(intent);

                    Statics.loadingResponse = true;

                } else {
                    Intent newIntent = new Intent(getActivity(), SearchIntentService.class);
                    newIntent.putExtra(Constants.GET_INFO_MOVIE, position);
                    newIntent.putParcelableArrayListExtra(Constants.RESULT_PARCELABLE, (ArrayList<Result>) results);
                    getActivity().startService(newIntent);
                }
            }
        }
    }

    protected void instantiateList(){
        if(resultBaseObservableList == null) {
            resultBaseObservableList = new ArrayList<>();
            resultBaseObservableList.clear();
        }

        if(moviesListAdapter == null) {
            moviesListAdapter = new MoviesListAdapter(resultBaseObservableList, this);
        }
    }

    public void setResults(List<Result> results) {
        instantiateList();

        if(results != null && results.size() > 0 && resultBaseObservableList != null && moviesListAdapter != null){
            resultBaseObservableList.clear();

            for(Result result : results){
                resultBaseObservableList.add(new ResultBaseObservable(result.getPoster(), result.getTitle(), result.getYear()));
            }

            moviesListAdapter.notifyDataSetChanged();
        }
    }
}
