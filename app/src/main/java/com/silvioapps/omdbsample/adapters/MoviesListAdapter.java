package com.silvioapps.omdbsample.adapters;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.silvioapps.omdbsample.BR;
import com.silvioapps.omdbsample.R;
import com.silvioapps.omdbsample.databinding.ResultBaseObservable;
import com.silvioapps.omdbsample.interfaces.IRecyclerViewOnClickListener;

import java.util.List;

public class MoviesListAdapter extends RecyclerView.Adapter<MoviesListAdapter.BindingViewHolder>{
    private List<ResultBaseObservable> resultBaseObservableList = null;
    private static IRecyclerViewOnClickListener recyclerViewClickListener = null;
    private BindingViewHolder viewHolder = null;

    public MoviesListAdapter(List<ResultBaseObservable> resultBaseObservableList, IRecyclerViewOnClickListener listener){
        this.resultBaseObservableList = resultBaseObservableList;
        this.recyclerViewClickListener = listener;
    }

    public static class BindingViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private ViewDataBinding viewDataBinding = null;

        public BindingViewHolder(View view) {
            super(view);

            view.setOnClickListener(this);

            viewDataBinding = DataBindingUtil.bind(view);
        }

        public ViewDataBinding getBinding() {
            return viewDataBinding;
        }

        @Override
        public void onClick(View view) {
            if(recyclerViewClickListener != null) {
                recyclerViewClickListener.recyclerViewListClicked(view, this.getLayoutPosition());
            }
        }
    }

    @Override
    public BindingViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.movies_list_layout,parent,false);
        viewHolder = new BindingViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(BindingViewHolder holder, int position) {
        if(resultBaseObservableList != null) {
            ResultBaseObservable resultBaseObservable = resultBaseObservableList.get(position);
            holder.getBinding().setVariable(BR.result, resultBaseObservable);
            holder.getBinding().executePendingBindings();
        }
    }

    @Override
    public int getItemCount(){
        return resultBaseObservableList == null ? 0 : resultBaseObservableList.size();
    }
}
