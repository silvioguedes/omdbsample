package com.silvioapps.omdbsample.adapters;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.silvioapps.omdbsample.R;
import com.silvioapps.omdbsample.constants.Constants;
import com.squareup.picasso.Picasso;

public class MovieImageBindingAdapter {
    @BindingAdapter({"imageUrl"})
    public static void loadImage(ImageView imageView, String url) {
        Picasso.with(imageView.getContext())
                .load(url)
                .placeholder(R.drawable.placeholder)
                .resize(Constants.WIDTH_IMAGE_THUMBNAIL, Constants.HEIGHT_IMAGE_THUMBNAIL)
                .centerInside()
                .into(imageView);
    }
}
